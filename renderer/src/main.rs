mod renderer;
use attheme_glossary_parser::parse;
use renderer::render;
use std::io::Write;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

fn main() {
    let mut red = ColorSpec::new();
    red.set_bold(true);
    red.set_fg(Some(Color::Red));

    let mut green = ColorSpec::new();
    green.set_bold(true);
    green.set_fg(Some(Color::Green));

    let glossary_dirs =
        std::fs::read_dir("../database")
            .unwrap()
            .filter_map(|entry| {
                let entry = entry.ok()?;
                let file_type = entry.file_type().ok()?;

                if file_type.is_dir() {
                    Some(entry)
                } else {
                    None
                }
            });

    let stderr = &mut StandardStream::stderr(ColorChoice::Always);
    let mut has_errored = false;

    let glossaries: Vec<_> = glossary_dirs
        .filter_map(|x| {
            let path = x.path();
            write!(stderr, "Parsing `{}`... ", path.to_string_lossy()).unwrap();
            stderr.flush().unwrap();

            match parse(&path) {
                Ok(glossary) => {
                    stderr.set_color(&green).unwrap();
                    writeln!(stderr, "Done").unwrap();
                    stderr.reset().unwrap();
                    let lang = path.file_name().unwrap().to_str().unwrap();

                    Some((lang.to_string(), glossary))
                }
                Err(errors) => {
                    has_errored = true;
                    stderr.set_color(&red).unwrap();
                    writeln!(stderr, "Failed:").unwrap();
                    stderr.reset().unwrap();
                    errors.iter().for_each(|error| println!("{:#?}", error));

                    None
                }
            }
        })
        .collect();

    if has_errored {
        std::process::exit(1);
    }

    writeln!(stderr).unwrap();

    let languages: Vec<_> = glossaries
        .iter()
        .map(|(lang, glossary)| (lang, &glossary.language_name))
        .collect();

    for (lang, glossary) in &glossaries {
        write!(stderr, "Rendering `{}`... ", lang).unwrap();
        render(lang, glossary, &languages);
        stderr.set_color(&green).unwrap();
        writeln!(stderr, "Done").unwrap();
        stderr.reset().unwrap();
    }
}
