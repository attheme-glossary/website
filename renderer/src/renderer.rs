use attheme_glossary_parser::{
    glossary::{Color, Contents, ParagraphItem, Section},
    Glossary,
};
use std::{collections::HashMap, fs, path::Path};
use tera::{Context, Tera};

type DefinedVariables = HashMap<String, (String, Color)>;

pub fn render(
    lang: &str,
    glossary: &Glossary,
    languages: &[(&String, &String)],
) {
    let content: String = glossary
        .sections
        .iter()
        .enumerate()
        .map(|(index, section)| {
            render_section(
                "",
                &section.section,
                2,
                index > 0,
                lang,
                &mut HashMap::new(),
            )
        })
        .collect();

    let mut context = Context::new();
    context.insert("lang", lang);
    context.insert("language_name", &glossary.language_name);
    context.insert("content", &content);
    context.insert("title", &glossary.title);
    context.insert("description", &glossary.about);

    let languages: Vec<_> = languages.iter().filter(|x| x.0 != lang).collect();

    context.insert("languages", &languages);

    let result = Tera::one_off(
        include_str!("../../frontend/index.html"),
        &context,
        false,
    )
    .expect("Invalid template");

    fs::create_dir_all("../rendered/").unwrap();
    fs::write(format!("../rendered/{lang}.html", lang = lang), result).unwrap();
}

fn create_target(title: &str) -> String {
    title.to_lowercase().replace(' ', "-")
}

fn render_section(
    prefix: &str,
    section: &Section,
    heading_level: u8,
    should_render_heading: bool,
    lang: &str,
    defined_variables: &mut DefinedVariables,
) -> String {
    let id = create_target(&section.title);
    let heading = if should_render_heading {
        let id = format!("{}{}", prefix, id);
        format!(
            "<a href=\"#{id}\"><h{n} id=\"{id}\">{title}</h{n}></a>",
            n = heading_level,
            id = id,
            title = section.title,
        )
    } else {
        String::new()
    };

    format!(
        "<section>
            {heading}
            {content}
        </section>",
        heading = heading,
        content = render_contents(
            &format!("{}{}/", prefix, id),
            &section.contents,
            heading_level,
            lang,
            defined_variables
        ),
    )
}

fn render_contents(
    prefix: &str,
    contents: &[Contents],
    heading_level: u8,
    lang: &str,
    defined_variables: &mut DefinedVariables,
) -> String {
    let iter = contents.iter();

    iter.map(|content| match content {
        Contents::Subsection(section) => render_section(
            prefix,
            section,
            heading_level + 1,
            true,
            lang,
            defined_variables,
        ),
        Contents::Paragraph(paragraph) => format!(
            "<p>{items}</p>",
            items = render_paragraph(&paragraph.items, lang, defined_variables)
        ),
        Contents::Variable {
            name,
            color,
            description,
            ..
        } => {
            let id = format!("{}{}", prefix, name.to_lowercase());
            let heading = format!(
                "<a href=\"#{id}\"><h{n}>{title}</h{n}></a>",
                id = id,
                n = heading_level + 1,
                title = render_variable_name(name),
            );

            let rendered = format!(
                "<article id=\"{id}\" class=\"{color_class}\">
                    {heading}
                    {content}
                    </article>",
                id = id,
                heading = heading,
                color_class = color_to_class(*color),
                content = render_contents(
                    &format!("{}{}/", prefix, name),
                    description,
                    heading_level + 1,
                    lang,
                    defined_variables
                ),
            );

            defined_variables.insert(name.to_string(), (id, *color));

            rendered
        }
        Contents::Image {
            url,
            alt,
            description,
            ..
        } => format!(
            "<figure>
                <img src=\"{src}\" alt=\"{alt}\"/>
                <figcaption>{caption}</figcaption>
            </figure>",
            src = Path::new(&format!("/{}/", lang))
                .join(url)
                .to_string_lossy(),
            alt = alt,
            caption =
                render_paragraph(&description.items, lang, defined_variables),
        ),
        Contents::List { start, items } => {
            let content: String = items
                .iter()
                .map(|content| {
                    let content = render_contents(
                        prefix,
                        content,
                        heading_level + 1,
                        lang,
                        defined_variables,
                    );
                    format!("<li><div>{content}</div></li>", content = content)
                })
                .collect();

            match start {
                Some(start) => format!(
                    "<ol start={start} style=\"--start: {start}\">
                        {content}
                    </ol>",
                    start = start,
                    content = content,
                ),
                None => format!(
                    "<ul>
                        {content}
                    </ul>",
                    content = content,
                ),
            }
        }
    })
    .collect()
}

fn render_paragraph(
    paragraph: &[ParagraphItem],
    lang: &str,
    defined_variables: &mut HashMap<String, (String, Color)>,
) -> String {
    paragraph
        .iter()
        .map(|item| match item {
            ParagraphItem::LineBreak => "<br/>".to_string(),
            ParagraphItem::Text(text) => text.to_string(),
            ParagraphItem::Bold(bold) => format!(
                "<strong>{bold}</strong>",
                bold = render_paragraph(bold, lang, defined_variables)
            ),
            ParagraphItem::Italic(italic) => format!(
                "<em>{italic}</em>",
                italic = render_paragraph(italic, lang, defined_variables)
            ),
            ParagraphItem::Monospace(code) => match defined_variables.get(code) {
                Some((link, color)) => format!(
                    "<a href=\"#{link}\" class=\"{color}\"><code>{variable}</code></a>",
                    link = link,
                    variable = render_variable_name(code),
                    color = color_to_class(*color),
                ),
                None => format!("<code>{code}</code>", code = render_variable_name(code)),
            },
            ParagraphItem::Strikethrough(strike) => format!(
                "<s>{strike}</s>",
                strike = render_paragraph(strike, lang, defined_variables)
            ),
            ParagraphItem::Link { text, url } => format!(
                "<a href=\"{href}\">{text}</a>",
                href = url,
                text = render_paragraph(text, lang, defined_variables)
            ),
        })
        .collect()
}

fn render_variable_name(name: &str) -> String {
    let mut rendered = String::new();
    let mut last_letter = '_';

    for letter in name.chars() {
        if letter.is_uppercase() && last_letter.is_lowercase() {
            rendered += "&shy;";
        }

        rendered.push(letter);
        last_letter = letter;
    }

    rendered
}

fn color_to_class(color: Color) -> &'static str {
    match color {
        Color::Red => "-red",
        Color::Pink => "-pink",
        Color::Purple => "-purple",
        Color::DeepPurple => "-deepPurple",
        Color::Indigo => "-indigo",
        Color::Blue => "-blue",
        Color::LightBlue => "-lightBlue",
        Color::Cyan => "-cyan",
        Color::Teal => "-teal",
        Color::Green => "-green",
        Color::LightGreen => "-lightGreen",
        Color::Lime => "-lime",
        Color::Yellow => "-yellow",
        Color::Amber => "-amber",
        Color::Orange => "-orange",
        Color::DeepOrange => "-deepOrange",
        Color::Brown => "-brown",
        Color::Gray => "-gray",
        Color::BlueGray => "-blueGray",
    }
}
