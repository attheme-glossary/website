#![feature(proc_macro_hygiene, decl_macro)]

use rocket::{get, response::NamedFile, response::Redirect, routes, State};
use rocket_accept_language::{AcceptLanguage, LanguageIdentifier};
use rocket_contrib::serve::StaticFiles;

#[get("/")]
fn index(
    accept_language: &AcceptLanguage,
    languages: State<Vec<String>>,
) -> Redirect {
    let accepted_languages = languages.inner();

    let languages: Vec<_> = accepted_languages
        .iter()
        .filter_map(|lang| {
            LanguageIdentifier::from_parts(Some(lang), None, None, &[]).ok()
        })
        .collect();

    match accept_language.get_appropriate_language_region(&languages[..]) {
        Some((lang, _)) => {
            if accepted_languages.contains(&lang.to_string()) {
                Redirect::to(format!("/{}", lang))
            } else {
                Redirect::to("/en")
            }
        }
        _ => Redirect::to("/en"),
    }
}

#[get("/<lang>")]
fn lang(lang: String, languages: State<Vec<String>>) -> Option<NamedFile> {
    if languages.inner().contains(&lang) {
        NamedFile::open(format!("../rendered/{lang}.html", lang = lang)).ok()
    } else {
        None
    }
}

fn main() {
    let languages: Vec<_> = std::fs::read_dir("../rendered")
        .unwrap()
        .filter_map(|x| x.ok())
        .filter_map(|x| {
            x.path().extension().and_then(|extension| {
                match extension.to_str() {
                    Some("html") => Some(x),
                    _ => None,
                }
            })
        })
        .map(|x| {
            let path = x.path();
            let file_name = path.file_stem().unwrap();

            file_name.to_str().unwrap().to_string()
        })
        .collect();

    let mut rocket = rocket::ignite()
        .mount("/", routes![index, lang])
        .manage(languages.clone())
        .mount("/static", StaticFiles::from("../frontend/static"));

    for lang in &languages {
        rocket = rocket.mount(
            &format!("/{lang}/images", lang = lang),
            StaticFiles::from(format!(
                "../database/{lang}/images",
                lang = lang
            )),
        )
    }

    rocket.launch();
}
